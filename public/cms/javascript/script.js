document.onload = selec()
document.getElementById('btnSchema').addEventListener('click', showShema)
function showShema() {
    document.getElementById('btnDoc').style.backgroundColor = "#A3B18A"
    document.getElementById('btnDoc').style.color = "#4A4A4A"
    document.getElementById('btnSchema').style.backgroundColor = "#588157"
    document.getElementById('btnSchema').style.color = "#FFFFFF"
    document.getElementById('shemaShow').classList.remove("hideContent")
    document.getElementById('docShow').classList.add("hideContent")
}
document.getElementById('btnDoc').addEventListener('click', showDoc)
function showDoc() {
    document.getElementById('btnSchema').style.backgroundColor = "#A3B18A"
    document.getElementById('btnSchema').style.color = "#4A4A4A"
    document.getElementById('btnDoc').style.backgroundColor = "#588157"
    document.getElementById('btnDoc').style.color = "#FFFFFF"
    document.getElementById('docShow').classList.remove("hideContent")
    document.getElementById('shemaShow').classList.add("hideContent")
}
function selec() {
    document.getElementById('btnSchema').style.backgroundColor = "#588157"
    document.getElementById('btnSchema').style.color = "#FFFFFF"
}