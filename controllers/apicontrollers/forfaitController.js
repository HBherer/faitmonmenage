const { ForfaitModel } = require('../../models/ModelForfait.js')

exports.forfait = (req, res, next) => {
    res.render('cms/forfait', { title: 'Faimonmenage | Forfait' });
}

exports.addforfait = (req, res, next) => {
    res.render('cms/add-content/addforfait');
}

exports.addUnforfait = async (req, res, next) => {
    try {
        let data = new ForfaitModel(req.body);
        data.save(function (err, data) {
            res.redirect('/forfait')
        })
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured ADD_EMPLOYE");
    }
}

exports.getForfait = async (req, res, next) => {
    try {
        let document = await ForfaitModel.find({});
        res.render('../views/cms/forfait', { forfaits: document, nombreForfait: document.length });
    } catch (err) {
        console.log(err);
        res.status(500).send(`aucun forfait n'a été trouvé`)
    }
}

exports.deleteForfaitByid = async (req, res, next) => {
    try {
        await ForfaitModel.findByIdAndRemove(req.params.id)
        res.redirect('/forfait')
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.updateForfait = async (req, res, next) => {
    try {
        await ForfaitModel.updateOne({ _id: req.params.id }, req.body)
        res.redirect('/forfait');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.getForfaitByid = async (req, res, next) => {
    try {
        let document = await getForfaitById(req.params.id);
        res.render('cms/edit-content/editforfait', { title: 'CMS | Éditer Forfait', forfaits: document });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to GET forfait CMS");
    }
}

const getForfaitById = async (id) => {
    return await ForfaitModel.findById(id);
}