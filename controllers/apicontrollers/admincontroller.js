const { AdminModel } = require('../../models/adminModel');
const { userModel } = require('../../models/userModel');
const bcrypt = require('bcrypt')

exports.login = async (req, res, next) => {
    try {
        res.render('cms/login', { title: 'Faimonmenage' });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured moderation to login");
    }
}

// Add admin un DB
exports.addAdmin = async (req, res, next) => {
    try {
        const hashed_password = await bcrypt.hash(req.body.password, 12);
        const user = new AdminModel({
            name: req.body.name,
            email: req.body.email,
            password: hashed_password
        });
        user.save((err, document) => {
            if (err) res.render('add', { error: err });
            res.render('cms/login', { data: document });
        });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.loginAdmin = async (req, res, next) => {
    try {
        const user = await AdminModel.findOne({ email: req.body.email });
        if (user) {
            let compare = await bcrypt.compare(req.body.password, user.password);
            if (compare) {
                req.session.authenticated = true
                req.session.email = req.body.email
                res.render('cms/', { title: "Faimonmenage | Accueil" })
            } else {
                res.render('cms/login', { title: "Faimonmenage | Connexion", erreur: "Mauvais nom d'utilisateur ou mot de passe" });
            }
        } else {
            res.render('cms/login', { title: "Faimonmenage | Connexion", erreur: "Mauvais nom d'utilisateur ou mot de passe" });
        }
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.logout = async (req, res, next) => {
    try {
        req.session.destroy((err) => {
            res.render('cms/login', { title: "Faimonmenage | Déconnexion", erreur: "Vous avez bien été déconnecté de votre session!" });
        })

    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to logout");
    }
}

exports.addpage = async (req, res, next) => {
    try {
        res.render('add', { title: 'Faimonmenage' });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured moderation to login");
    }
}

exports.editUser = async (req, res, next) => {
    const user = await userModel.findById(req.params.id)
    res.render('cms/edit-content/editutilisateur', { title: "Faitmonmenage | Éditer utilisateur", user })
}

exports.deleteUser = async (req, res, next) => {
    try {
        await userModel.findByIdAndRemove(req.params.id)
        res.redirect('/utilisateur-p')
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.updateUser = async (req, res, next) => {
    try {
        await userModel.updateOne({ _id: req.params.id }, req.body)
        res.redirect('/utilisateur-p');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}