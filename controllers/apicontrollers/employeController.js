const { EmployeModel } = require('../../models/ModelEmploye.js')
const ModelEmploye = require('../../models/apiModel.js')
const { userModel } = require('../../models/userModel')
const { ForfaitModel } = require('../../models/ModelForfait')
const { model } = require('mongoose')

exports.index = async (req, res, next) => {
    try {
        const user = await userModel.findOne({ email: req.session.email })
        const numberParticular = await userModel.count({ "type": "particular" }),
            numberBusiness = await userModel.count({ "type": "business" }),
            numberEmployees = await EmployeModel.count(),
            numberForfait = await ForfaitModel.count()
        res.render('cms/index', { title: 'Faimonmenage', numberParticular, numberBusiness, numberEmployees, numberForfait, user })
    } catch (e) {
        console.log(e)
        res.status(500).send("Internal Server error occured");
    }
}

exports.utilisateur = (req, res, next) => {
    res.render('cms/utilisateur', { title: 'Faimonmenage | Utilisateur' });
}

exports.entreprise = async (req, res, next) => {
    try {
        const users = await userModel.find({ type: "business" })
        let number = users.length
        for (let k in users) {
            if (JSON.parse(users[k].hire).length != 0) {
                users[k].hire = "Oui"
            } else { users[k].hire = "Non" }
        }
        res.render('cms/entreprise', { title: 'Faimonmenage | Entreprise', users, number })
    } catch (e) {
        console.log(e)
        res.status(500).send("Internal Server error occured");
    }
}

exports.particulier = async (req, res, next) => {
    try {
        const users = await userModel.find({ type: "particular" })
        let number = users.length
        for (let k in users) {
            if (JSON.parse(users[k].hire).length != 0) {
                users[k].hire = "Oui"
            } else { users[k].hire = "Non" }
        }
        res.render('cms/particulier', { title: 'Faimonmenage | Particulier', users, number })
    } catch (e) {
        console.log(e)
        res.status(500).send("Internal Server error occured");
    }
}

exports.employe = (req, res, next) => {
    res.render('cms/employe', { title: 'Faimonmenage | Employer', employees })
}

exports.forfait = (req, res, next) => {
    res.render('cms/forfait', { title: 'Faimonmenage | Forfait' });
}
exports.horraire = async (req, res, next) => {
    employees = await EmployeModel.find({})
    res.render('cms/horaire', { title: 'Faimonmenage | Horaire', employees });
}
exports.documetation = (req, res, next) => {
    res.render('cms/documentation', { title: 'Faimonmenage | Documentation' });
}
exports.profil = async (req, res, next) => {
    try {
        const users = await userModel.findById(req.params.id)
        if (JSON.parse(users.hire).length != 0) {
            users.hire = "Oui"
        } else { users.hire = "Non" }
        res.render('cms/profil', { title: 'Faimonmenage | Profil', users });
    } catch (e) {
        console.log(e);
        res.status(500).send("Internal Server error occured");
    }
}

exports.addemploye = (req, res, next) => {
    res.render('cms/add-content/addemploye');
}

exports.addOneEmploye = async (req, res, next) => {
    try {
        let data = new EmployeModel(req.body);
        data.save(function (err, data) {
            res.redirect('/employe')
        })
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured ADD_EMPLOYE");
    }
}

exports.getEmployes = async (req, res, next) => {
    try {
        let document = await EmployeModel.find({});
        res.render('../views/cms/employe', { employes: document, nombreEmploye: document.length });
    } catch (err) {
        console.log(err);
        res.status(500).send(`aucun employer n'a été trouvé`)
    }
}

exports.gethoraireEmployes = async (req, res, next) => {
    try {
        let document = await EmployeModel.find({});
        res.render('../views/cms/horaire', { employes: document });
    } catch (err) {
        console.log(err);
        res.status(500).send(`aucun employer n'a été trouvé`)
    }
}

exports.getEmployeByid = async (req, res, next) => {
    try {
        let document = await getEmployeById(req.params.id);
        res.render('cms/edit-content/editemploye', { title: 'CMS | Éditer Employer', employes: document });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured to GET employer CMS");
    }
}

exports.updateEtatEmploye = async (req, res, next) => {
    try {
        await EmployeModel.updateOne({ _id: req.params.id }, req.body)
        res.redirect('/employe');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.updateHoraire = async (req, res, next) => {
    try {
        await EmployeModel.updateOne({ _id: req.params.id }, req.body)
        res.redirect('/horaire');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.deleteEmployeByid = async (req, res, next) => {
    try {
        await EmployeModel.findByIdAndRemove(req.params.id)
        res.redirect('/employe')
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

exports.updateEmploye = async (req, res, next) => {
    try {
        await EmployeModel.updateOne({ _id: req.params.id }, req.body)
        res.redirect('/employe');
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}

const getEmployeById = async (id) => {
    return await EmployeModel.findById(id);
}

exports.getAllEmployes = async (req, res, next) => {
    try {
        let document = await ModelEmploye.getAllEmployes();
        res.json(document)
    } catch (err) {
        console.log(err);
        res.status(500).send('aucun employés trouvés')
    }
}

exports.getEmployeUnById = async (req, res, next) => {
    try {
        let document = await ModelEmploye.getEmployeById(req.params.id)
        res.json(document)
    } catch (err) {
        console.log(err)
        res.status(500).send('Employe non trouvé')
    }
}

exports.putEmployeByEtat = async (req, res, next) => {
    try {
        let data = req.body;
        for (let k in data.etat) {
            await ModelEmploye.updateOneEmployeEtat(data.etat[k], "Indisponible")
        }
        console.log(req.params.id)
        console.log(typeof JSON.stringify(data.etat))
        await userModel.updateOne({ _id: req.params.id }, { hire: JSON.stringify(data.etat) })
        res.json({ "ok": true });
    } catch (err) {
        console.log(err);
        res.status(500).send('Employé non trouvé')

    }
}