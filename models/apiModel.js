var Mongoose = require("mongoose");

var Schema = Mongoose.Schema;
let EmployeSchema = new Schema({
    nom: String,
    prenom: String,
    etat: String,
    hire: String
});

var EmployeModel = Mongoose.model("Employes", EmployeSchema);

module.exports = {
    model: EmployeModel,
    getAllEmployes: async () => {
        return await EmployeModel.find({ etat: "Disponible" })
    },
    getEmployeById: async (id) => {
        return await EmployeModel.findById(id)
    },
    updateOneEmployeEtat: async (id, data) => {
        return await EmployeModel.updateOne({ _id: id }, { etat: data })

    }

}
