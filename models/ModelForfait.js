var Mongoose = require("mongoose");

var Schema = Mongoose.Schema;
let ForfaitSchema = new Schema({
    recurrence: String,
    frequence: String,
});

exports.ForfaitModel = Mongoose.model("Forfait", ForfaitSchema);
