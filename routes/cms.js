var express = require('express');
var router = express.Router();
var admincontroller = require('../controllers/apicontrollers/admincontroller.js')
var employeController = require('../controllers/apicontrollers/employeController.js')
var forfaitController = require('../controllers/apicontrollers/forfaitController.js')
var auth = require('../auth/auth')

// GET Page du CMS
router.get('/', auth, employeController.index)
router.get('/utilisateur', auth, employeController.utilisateur)
router.get('/utilisateur-e', auth, employeController.entreprise)
router.get('/utilisateur-p', auth, employeController.particulier)
router.get('/employe', auth, employeController.getEmployes)
router.get('/horaire', auth, employeController.gethoraireEmployes)
router.get('/forfait', auth, forfaitController.getForfait)
router.get('/profil/:id', auth, employeController.profil)
router.get('/documentation', auth, employeController.documetation)
router.get('/addemploye', auth, employeController.addemploye)
router.get('/addforfait', auth, forfaitController.addforfait)

router.get('/delet-utilisateur/:id', auth, admincontroller.deleteUser)
router.get('/utilisateur-edit/:id', auth, admincontroller.editUser)

router.get('/login', admincontroller.login)
router.get('/logout', auth, admincontroller.logout)

router.get('/forfait/delete/:id', auth, forfaitController.deleteForfaitByid)
router.get('/forfait/editforfait/:id', auth,  forfaitController.getForfaitByid);

router.get('/employe/editemploye/:id', auth,  employeController.getEmployeByid);
router.get('/employe/delete/:id', auth, employeController.deleteEmployeByid)

// POST
router.post('/login', admincontroller.loginAdmin)

router.post('/add-employe', auth, employeController.addOneEmploye)
router.post('/employe/editemploye/:id', auth,  employeController.updateEmploye);
router.post('/employe/delete/:id', auth, employeController.deleteEmployeByid)

router.post('/horaire/:id', auth, employeController.updateHoraire)

router.post('/add-forfait', auth, forfaitController.addUnforfait)
router.post('/forfait/editeforfait/:id', auth, forfaitController.updateForfait)
router.post('/forfait/delete/:id', auth, forfaitController.deleteForfaitByid)

router.post('/utilisateur-edit/:id', auth, admincontroller.updateUser)
router.post('/delet-utilisateur/:id', auth, admincontroller.deleteUser)

//Add Admin
router.post('/add-admin', admincontroller.addAdmin)
router.get('/addPage', admincontroller.addpage)

module.exports = router;

