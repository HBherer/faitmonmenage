var express = require('express');
var router = express.Router();
var employeController = require('../controllers/apicontrollers/employeController')

// GET
router.get('/', employeController.getAllEmployes)
router.get('/:id', employeController.getEmployeUnById)

// PUT
router.put('/:id', employeController.putEmployeByEtat)

module.exports = router;
